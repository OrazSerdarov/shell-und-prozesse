#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>

int kindprozesseIDs [3];
void doVater(void);

int main(int argc, char** argv) {

    int cpid;
    int i = 1;

    if(argc==5){
    printf("---Vaterprozess(%d)---\n",getpid());
    printf(" %i. Übergabeargument: %s \n\n", i ,argv[i]);
    
    while (i < 4) {
        switch (cpid = fork()) {
            case -1: 
		printf("Fehler bei %d. fork() .............\n", i);
                break;
            case 0:
                printf("---%d. Kindprozess(%d)---Vater(%d)\n", i, getpid(), getppid());
                printf(" %i. Übergabeargument: %s \n \n", i + 1, argv[i + 1]);
                if (i == 1 || i == 2) {
                    //1. oder 2. Kindprozess
                    while (1);
                } else {
                    //3. Kindprozess
                    sleep(1);
                    exit(2);

                }
                break;
            default:
                //Im Vaterprozess
                kindprozesseIDs[i - 1] = cpid;
                if (i == 3) {
                    doVater();
                }
                break;



        }
        i++;


    }
    
    printf("Vaterprozess wird beendet... \n");
    }else{
	printf("Geben Sie bitte genau 4 Aufrufparameter an! \n");	

	}


    return (EXIT_SUCCESS);
}

void doVater(void){
    
    int status;
    int cpid;

    sleep(2);
    kill(kindprozesseIDs[0], SIGTERM);
    kill(kindprozesseIDs[1], SIGKILL);

    for(int i =0; i<3;i++){      
       	cpid=wait(&status);
	printf("Kindprozess mit ID %d und Status %d. \n", cpid, status);
    }
}
