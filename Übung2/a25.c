#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
//#include <signal.h>
#include <sys/wait.h>
#include <dirent.h>
#include <stdbool.h>
#include <string.h>

#define MAX 255




bool containsAbsolutePath(char *);
bool parseInput(char *, char *, char*);
char ** parseEnvPath();
int countPaths(char ** paths);

int main(int argc, char* argv[]) {

    char currentDirectory[MAX];
    char input[MAX];
    char progName[MAX];
    char params[MAX];
    currentDirectory[MAX - 1] = '\0';
    input[MAX - 1] = '\0';
    progName[MAX - 1] = '\0';
    params[MAX - 1] = '\0';
    bool weiter = true;
    int cpid;
    int status;


    printf("Programm A25(Midi-Shell) wird gestartet...\n");

    if (getcwd(currentDirectory, MAX - 1) == NULL) {
        printf("Aktuelles Arbeitsverzeichnis konnte nicht ermittelt werden! \n"
                "Programm wird beendet!");
        weiter = false;
    } else {
        printf("Aktuelles Arbeitsverzeichnis ist : %s \n", currentDirectory);
    }

    while (weiter) {

        fgets(input, MAX, stdin);

        if (parseInput(input, progName, params) == 0) {
            weiter = false; //schluss eingegeben
        } else {
            //Hier Programm starten
            cpid = fork();
            if (cpid == 0) {
                //Im Kindprozess
                if (containsAbsolutePath(input)) {
                    //Kann gleich starten
                    execl(input, "P-neu", NULL);
                    printf("Es gibt keinen Programm mit dem Namen '%s' \n", input);
                    exit(EXIT_FAILURE);
                } else {
                    //progName und params nutzen
                    char ** allPaths;
                    parseEnvPath(allPaths);
                    int numPaths = countPaths(allPaths);

                    if (numPaths == 0) {
                        printf("Programm  '%s' wurde nicht gefunden!\n", progName);
                    } else if (numPaths == 1) {
                        //genau ein Programm mit dem eingegebenen Namen vorhanden

                    } else {
                        //mehr als ein Programm mit dem eingegebenen Namen vorhanden
                        int progNum = 0;
                        printf("Es gibt mehrere Programme mit dem Programmnamen '%s' !\n", progName);
                        for (int i = 0; i < numPaths; i++) {
                            printf("%d: %s \n", i, paths[i]);
                        }
                        printf("Welches Programm wollen Sie starten?\n "
                                "Geben Sie eine Zahl!");
                        scanf("%d", &progNum);

                        printf("Programm '%s' im Verzeichnis '%s' wird gestartet...\n", progName, paths[progNum]);
                        //if() mit Parametern...



                    }
                    

                    exit(EXIT_FAILURE);
                }

            } else {
                //Im Elternprozess	
                wait(&status);
                printf("Der Kindprozess mit der ID %d und der StatusID %d hat sich beendet! \n", cpid, status);

            }

        }

    }


    return EXIT_SUCCESS;
}

bool parseInput(char * input, char * progName, char* param) {
    bool schluss = true;
    int strLength = 0;
    input[strcspn(input, "\n")] = 0; //removes newline
    schluss = strcmp(input, "schluss");


    if (schluss == 0) {//schluss eingegeben
        return schluss;
    } else {
        return 1;
    }


}

void parseEnvPath(char ** allPaths) {
    char * env = getenv("PATH");
    char *aPath = NULL;
    
    for(int i=0 ;i<countEnvVar();i++){
        aPath = strsep(&env, ":");
        allPaths[i]=aPath;
    }

}


int countEnvVar(){
    char * env = getenv("PATH");
    int i =0;
     while (strsep(&env, ":") != NULL){
         i++;
     } 
    return i;
}

int countPaths(char ** paths) {
    int i = 0;
    while (paths[i] != NULL) {
        i++;
    }
    return i;
}

//  

bool containsAbsolutePath(char * input) {
    bool ret = false;
    bool con1 = (input[0] == '/');
    bool con2 = (input[0] == '.' && con1);
    bool con3 = (input[0] == '.' && con2);

    return con1 || con2 || con3;
}

