#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/wait.h>
#include <dirent.h>

#define MAX 255

int main(int argc, char* argv[]) {
    char aktVerzeichnis[MAX];
    int cpid;
    int status;

    aktVerzeichnis[MAX - 1] = '\0';
    printf("\nProgramm A24(Starten) wird gestartet...\n");
    if (getcwd(aktVerzeichnis, MAX - 1) == NULL) {
        printf("Aktuelles Arbeitsverzeichnis konnte nicht ermittelt werden! \n");
    }
   
	cpid=fork();
 
    if (cpid ==0) {
        //im Kindprozess

        execl("./HelloWorld", "P-neu" , NULL);


        printf("Das P-neu konnte nicht gestartet werden! \n");



    }

    //im Elternprozess warten bis Kind durch ist
    waitpid(cpid, &status, NULL);
    printf("Der Kindprozess mit der ID %d und der StatusID %d hat sich beendet! \n", cpid, status);


    return EXIT_SUCCESS;
}



