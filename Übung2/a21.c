#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char** argv) {
    
    int cpid= fork();
    
    if(cpid==0){
        //Kindprozess
        printf("---Im Kindprozess---\n" );
        printf(" Mein  Prozessnummer: %d \n" , getpid() );
        printf(" Vater Prozessnummer: %d \n" , getppid() );         
        while(1);
    }else{
        //Vaterprozess
         printf("---Im Vaterprozess--- \n" );
         printf(" Mein  Prozessnummer: %d \n" , getpid() );
         printf(" Vater Prozessnummer: %d \n" , getppid() );         
        while(1);
    }
           

    return (EXIT_SUCCESS);
}

