#include <stdio.h>
#include <stdlib.h>

#define MAX_CHARACTERS 100 

int getSize(char*);

int main(int argc, char* argv[]){

	char* param= argv[1];
	int NUM_NAMES  = (*param)-48;	
	int i =0;
	char aName[MAX_CHARACTERS];
	char* pointer=NULL; 
	printf("Geben Sie einen Namen ein \n");
		
	while(i<NUM_NAMES){
	
		if(fgets(aName,MAX_CHARACTERS,stdin)!=NULL){
			int end = getSize(aName);
			pointer=aName+ end;

			while(end>=0){
				printf("%c", *pointer);
				pointer--;
				end--;
			
			}
			printf("\n");
				
		}else{
			printf("Sie haben Quatsch eingegeben!");
		}

		i++;
	}

	return 0;

}

int getSize(char* arr){
	char* pointer =arr;
	int size=0;
		while(*pointer!=NULL){
			size++;
			pointer++;			
		}
 
	return size;
}
