Dies ist eine Dokumentation zu Aufgabe 31 Shup SS2020.
Dokumentiret wird nur die Codeabschnitte, die für die Synchronisation der
Prozesse ( Main bzw. Anwendungen die Main startet, Spooler und Drucker).

Anwendung - Spooler teilen :
	-eine Semaphorenmenge der Größe 3 (mutex,leer,voll)
	-Shared Memory der Größe 6 
	-Letzte 4 Byte der Shared Memory sind für den Index(nextWrite) gedacht 

Spooler - Printer1 bzw Spooler -Printer  teilen:
	-eine Semaphorenmenge der Größe 3 (mutex,leer,voll)
	-Shared Memory der Größe 2

Folgend werden einfacherhalber nur wait(mutex,voll,leer) und signal(mutex,voll,leer) zur Dokumentation
der Synchronisation verwendet. Sonst aufs Kontext schauen.


Main:

 createShmSpooler();
 createSemSpooler();

      while(!interruptOccured){
              startProcess();	
      }

 stopStartedProcesses();
 stopSpooler()
 deleteShmSpooler();
 deleteSemSpooler();
 
Anwendung:
    
     crateMsg();
     wait(leer);
     wait(mutex);
     getNextIndexShmSpooler();
     writeMsgToShmSpoolerAtNextIndex();
     setNextIndexShmSpooler();
     signal(mutex);
     signal(voll);
     




Spooler:				

 createShmPrinter();						 
 createSemPrinter();			
 createPrinter1();			
 createPrinter2();
 while(!spoolerInterrupt){
          wait(voll);
          wait(mutex);
          readNextMsgFromShmSpooler();	     
          signal(mutex);
          signal(leer);

          wait(leer);   //nextPrinter
          wait(mutex);
          writeNextMsgToShmPrinter();			
          signal(mutex);
          signal(voll);	  
         }
deleteShmPrinter();
deleteSemPrinter();
stopPrinters()



Drucker1 und Drucker2:

           while(!printInterrupt){
	wait(voll);
	wait(mutex);	
	readMsgFromShmPrinter();
	signal(mutex);
	signal(leer);
	print(); 
           }
