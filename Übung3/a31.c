#include <stdio.h>
#include <unistd.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <stdbool.h>
#include <string.h>
#include "functions.h"
#include "signalhandler.h"
#include "semap.h"


#define MUTEX 0
#define LEER 1
#define VOLL 2
#define PERMI 0777 

#define KEY_SEM_SPOOLER 987458L
#define KEY_SEM_Printer1 823458L
#define KEY_SEM_Printer2 127758L

#define SHMDATASPOOLER 6 *sizeof(int)
#define SHMDATAPRINTER 2 * sizeof(int)

int main(int argc, char** argv) {


    setMainSighandler();
    initRandom();
    printf("Programm Druckerspooler wird gestartet...\n"
            "Beenden Sie die Simulation mit SIGTERM an (%d) \n"
            "================================================\n", getpid());


    int spoolerId;
    int shm_spooler = shmget(IPC_PRIVATE, SHMDATASPOOLER, IPC_CREAT | IPC_EXCL | 0777);
    int* shmprt_spooler = (int*) shmat(shm_spooler, NULL, 0);
    int sem_spooler;


    if (*shmprt_spooler == -1 || init_semaphore(&sem_spooler, KEY_SEM_SPOOLER, 5) == -1) {
        printf("Ein Fehler ist aufgetreten! Status:%d \n", errno);
        return (EXIT_FAILURE);
    } else {




        if ((spoolerId = fork()) == 0) {
            printf("-----Spooler(%d) wird gestartet-----\n", getpid());
            setSpoolerSigHandler();
            int shm_printer = shmget(IPC_PRIVATE, SHMDATAPRINTER, IPC_CREAT | IPC_EXCL | 0777);
            int* shmprt_printer = (int*) shmat(shm_printer, NULL, 0);
            int sem_printer1;
            int sem_printer2;
            init_semaphore(&sem_printer1, KEY_SEM_Printer1, 1);
            init_semaphore(&sem_printer2, KEY_SEM_Printer2, 1);

            int printerIds[2];
            for (int i = 0; i < 2; i++) {
                switch (printerIds[i] = fork()) {
                    case -1:
                        printf("Fehler bei fork() für %d.Drucker.............\n", i);
                        break;
                    case 0:
                        setPrinterSigHandler();
                        printf("-----%d. Drucker(%d) wird gestartet-----\n\n", i + 1, getpid());

                        while (!printerInterrupt) {
                            if (i == 0) {
                                int toSleep;
                                int msg;
                                waitSem(&sem_printer1, VOLL);
                                waitSem(&sem_printer1, MUTEX);
                                msg =shmprt_printer[i];
                                signalSem(&sem_printer1, MUTEX);
                                signalSem(&sem_printer1, LEER);
                                toSleep = printPages();
                                sleep(toSleep);
                                printf("------------------------------------------------------------Drucker1(%d) druckt %d min : %d  \n\n", getpid(), toSleep, msg);
                            } else {
                                int toSleep;
                                int msg;
                                waitSem(&sem_printer2, VOLL);
                                waitSem(&sem_printer2, MUTEX);
                                msg =shmprt_printer[i];
                                signalSem(&sem_printer2, MUTEX);
                                signalSem(&sem_printer2, LEER);
                                toSleep = printPages();
                                sleep(toSleep);
                                printf("------------------------------------------------------------Drucker2(%d) druckt %d min : %d  \n\n", getpid(), toSleep,msg);

                            }

                        }
                        exit(EXIT_SUCCESS);
                        break;
                    default:
                        break;
                }

            }



            int nextRead = 0;
            int nextPrinter = 0;
            int msg = 0;

            while (!spoolerInterrupt) {

                waitSem(&sem_spooler, VOLL);
                waitSem(&sem_spooler, MUTEX);
                msg = *(shmprt_spooler + nextRead);
                signalSem(&sem_spooler, MUTEX);
                signalSem(&sem_spooler, LEER);
                printf("++++++++++++++++++++++++++++++++++++++++++Spooler liest MSG : %d   \n\n", msg);
                nextRead = nextRead + 1;
                if (nextRead == 5) {
                    nextRead = 0;
                }


                if (nextPrinter == 0) {

                    waitSem(&sem_printer1, LEER);
                    waitSem(&sem_printer1, MUTEX);
                    shmprt_printer[nextPrinter] = msg;
                    signalSem(&sem_printer1, MUTEX);
                    signalSem(&sem_printer1, VOLL);
                    nextPrinter = 1;
                } else {
                    waitSem(&sem_printer2, LEER);
                    waitSem(&sem_printer2, MUTEX);
                    shmprt_printer[nextPrinter] = msg;
                    signalSem(&sem_printer2, MUTEX);
                    signalSem(&sem_printer2, VOLL);

                    nextPrinter = 0;
                }


            }
            //schauen ob noch was zum Drucken gibt
            int status [2];
            if (kill(printerIds[0], SIGTERM) == 0) {
                waitpid(printerIds[0], status[0], NULL);
                printf("1.Drucker(%d) hat sich beendet! Status:%d \n", printerIds[0], status[0]);
            }
            if (kill(printerIds[1], SIGTERM) == 0) {
                waitpid(printerIds[1], status[1], NULL);
                printf("2.Drucker(%d) hat sich beendet! Status:%d \n", printerIds[1], status[1]);
            }

            deleteShm(&shm_printer);
            deleteSem(&sem_printer1);
            deleteSem(&sem_printer2);
            exit(EXIT_SUCCESS);

        } else {
            //IM Vater
            printf("\n============================================\n"
                    "Anwendungen werden gestartet"
                    "\n===========================================\n");


            int cpid [20] = {0};
            int i = 0;


            *(shmprt_spooler + 5) = 0;

            while (!mainInterrupt) {
                if ((cpid[i++] = fork()) == 0) {
                    int nextWrite = 0;
                    int message = createMsg();

                    waitSem(&sem_spooler, LEER);
                    waitSem(&sem_spooler, MUTEX);
                    nextWrite = *(shmprt_spooler + 5);
                    *(shmprt_spooler + nextWrite) = message;
                    printf("==>%d.Process(%d) schreibt : %d\n\n", i, getpid(), message);
                    nextWrite = nextWrite + 1;
                    if (nextWrite == 5) {
                        *(shmprt_spooler + 5) = 0;
                    } else {
                        *(shmprt_spooler + 5) = nextWrite;
                    }
                    signalSem(&sem_spooler, MUTEX);
                    signalSem(&sem_spooler, VOLL);
                    exit(EXIT_SUCCESS);
               
                }  else {
                        if (i == 20) {
                            i = 0;
                        }
                        sleep(waitSeconds());

                    }
                }

                stopProcesses(cpid, 20);
                stopSpooler(spoolerId);
                deleteShm(&shm_spooler);
                deleteSem(&sem_spooler);

                printf("=======================Simulation ist beendet=======================\n");
                return (EXIT_SUCCESS);
            }





        }
    }







