
#include <errno.h>




// 3 sem mutex leer ,voll
int init_semaphore(int * sem ,int key, int leer ) {
    *sem = semget(key, 0, IPC_PRIVATE);
    if (*sem < 0) {
        umask(0);
        *sem = semget(key, 3, IPC_CREAT | IPC_EXCL | 0777);
        if (*sem < 0) {
            printf("Fehler beim Anlegen des Semaphors %d ...\n", errno);
            return -1;
        }
        printf("Semaphor-ID(%d) angelegt!\n", *sem);
        if (semctl(*sem, 0, SETVAL, (int) 1) == -1) {
            printf("Mutex konnte nicht initialisiert werden!\n");
            return -1;

        } else {
            if (semctl(*sem, 1, SETVAL, (int) leer) == -1) {
                printf("Leer konnte nicht initialisiert werden!\n");

            } else {
                if (semctl(*sem, 2, SETVAL, (int) 0) == -1) {
                    printf("Voll konnte nicht initialisiert werden!\n");
                } else {
                    printf("Initialisierung der Semaphore war erfolgreich!\n");
                    return 1;
                }
            }
        }

    }
     printf("Semaphor-ID(%d) bereits existiert!\n", *sem);
    return 1;
}

void waitSem(int* sem ,int sem_num){
    struct sembuf sb;
    sb.sem_num = sem_num;
    sb.sem_op = -1;
    sb.sem_flg = 0;
    semop((*sem), &sb, 1);
}

void signalSem(int* sem , int sem_num) {
    struct sembuf sb;
    sb.sem_num = sem_num;
    sb.sem_op = 1;
    sb.sem_flg = 0;
    semop((*sem), &sb, 1);

}

