#include <signal.h>
// Signalhandler



bool mainInterrupt = false;
bool spoolerInterrupt = false;
bool printerInterrupt = false;

void printerSignalhandler(int sigtype) {
    if (sigtype == SIGTERM) {
        printf("\nSIGTERM in Drucker ist aufgetreten! \n");
        printerInterrupt = true;
    }
}

void spoolerSignalhandler(int sigtype) {
    if (sigtype == SIGTERM) {
        printf("\nSIGTERM in Spooler ist aufgetreten! \n");
        spoolerInterrupt = true;
    }
}

void mainSignalhandler(int sigtype) {
    if (sigtype == SIGTERM) {
        printf("\nSIGTERM in in Main ist aufgetreten!\n");
        mainInterrupt = true;
    }
    if (sigtype == SIGCHLD) {
        printf("\n SIGCHLD in in Main ist aufgetreten!\n");

    }
}

void setMainSighandler() {
    struct sigaction action;
    action.sa_handler = mainSignalhandler;
    sigemptyset(&action.sa_mask);
    action.sa_flags = SA_RESETHAND && SA_NOCLDSTOP;
    sigaction(SIGTERM, &action, NULL);
    action.sa_handler = SIG_IGN;
    sigaction(SIGCHLD, &action, NULL);

}

void setSpoolerSigHandler() {
    struct sigaction action;
    action.sa_handler = spoolerSignalhandler;
    sigaction(SIGTERM, &action, NULL);
}

void setPrinterSigHandler() {
    struct sigaction action;
    action.sa_handler = printerSignalhandler;
    sigaction(SIGTERM, &action, NULL);
}

