#include <sys/ipc.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/wait.h>




extern bool mainInterrupt;
extern bool spoolerInterrupt;
extern bool printerInterrupt;

int createMsg() {
    return rand() % RAND_MAX;
}

void initRandom(void) {
    time_t t;
    srand((unsigned) time(&t));
}

int waitSeconds(void) {

    return rand() % 5;
}

int printPages(void) {

    double r = 7 - 2 + 1;
    return 2 + (int)(r * rand()/(RAND_MAX+1.0));

    //    return (rand() % 5) + 2;
}

void stopSpooler(int spoolerId) {

    int status;
    if (kill(spoolerId, SIGTERM) == 0) {
        printf("Signal an Spooler(%d) geschickt ! \n", spoolerId);
        waitpid(spoolerId, &status, NULL);
        printf(">>> Spooler(%d) hat sich beendet! Status:%d\n", spoolerId, status);
    } else {
        printf("Signal an Spooler(%d) konnte nicht geschickt  werden !\n", spoolerId);
    }
}

void stopProcesses(int *cpid, int size) {

    int status[size];
    for (int j = 0; j < size && cpid[j] != 0; j++) {
        if (kill(cpid[j], SIGTERM) == 0) {
            waitpid(cpid[j], status[j], NULL);
            printf("Process(%d) wurde beendet! Status: %d \n", cpid[j], status[j]);
        }
    }


}

void deleteShm(int * shm) {
    int sh = *shm;
    if (shmctl((*shm), IPC_RMID, 0)== 0) {
        printf("\nShared Memory(%d) erfolgreich gelöscht! \n", sh );
    } else {
        printf("Fehler beim Löschen von Shared Memory!  \n");

    }
}

void deleteSem(int *sem) {
    int semap = *sem;
    if (semctl((*sem), 0, IPC_RMID, 0)== 0) {
        printf("\nSemaphore(%d) erfolgreich gelöscht! \n" , semap);

    } else {
        printf("Fehler beim Löschen von Semaphore !  \n");
    }
}
