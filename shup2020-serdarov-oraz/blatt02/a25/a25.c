#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
//#include <signal.h>
#include <sys/wait.h>
#include <dirent.h>
#include <stdbool.h>
#include <string.h>


#include "a25.h"

int main(int argc, char* argv[]) {
    printf("Diese Lösung wurde erstellt von Oraz Serdarov \n\n");

    char currentDirectory[MAX];
    currentDirectory[MAX - 1] = '\0';
    bool cont = true;
    int cpid;
    int status;


    printf("Programm A25(Midi-Shell) wird gestartet...\n");

    if (getcwd(currentDirectory, MAX - 1) == NULL) {
        printf("Aktuelles Arbeitsverzeichnis konnte nicht ermittelt werden! \n"
                "Programm wird beendet!");
        cont = false;
    } else {
        printf("Aktuelles Arbeitsverzeichnis ist : %s \n", currentDirectory);
    }
       struct ProgStruct aProgStruct;
        char ** progParams = malloc(10 * sizeof (char*));
        char ** progPaths = malloc(10 * sizeof (char*));
        for (int i = 0; i < 10; i++) {
            progParams[i] = malloc(MAX * sizeof (char));
            progPaths[i] = malloc(MAX * sizeof (char));
        }

    while (cont) {


     
        initializeStruct(&aProgStruct);
        parseInput(&aProgStruct, progParams);
        progPaths = findAllProg(&aProgStruct, progPaths);
  
        
        

        if (aProgStruct.end == true) {
            cont = false; 
        } else {
        printf("Progpath  %s   %p \n", *progPaths , progPaths);
            cpid = fork();
            if (cpid == 0) {
           
                if (aProgStruct.containsAbsolutePath == true) {
                    execl(aProgStruct.progName, "P-neu", NULL);
                    printf("Es gibt keinen Programm mit dem Namen '%s' \n", aProgStruct.progName);
                    exit(EXIT_FAILURE);
                } else {

                    if (aProgStruct.numOfPathsFound == 0) {
                        printf("Programm  '%s' wurde nicht gefunden!\n", aProgStruct.progName);
                    } else if (aProgStruct.numOfPathsFound == 1) {
                        printf("Zusammenf %s    %s!\n", progPaths[0], aProgStruct.progName);
                        char * progStart = strcat(progPaths[0], aProgStruct.progName);
                        printf("Programm  '%s'  wrid gestartet !\n", progStart);
                        execl(*progStart, "P-neu", NULL);
                        printf("Programm konnte nicht '%s'  gestartet werden!\n", aProgStruct.progName);
                        exit(EXIT_FAILURE);

                    } else {
              
                        int progNum;
                        printf("Es gibt mehrere Programme mit dem Programmnamen '%s' !\n", aProgStruct.progName);
                   
                        printf("Welches Programm wollen Sie starten?\n "
                                "Geben Sie eine Zahl!");
                        scanf("%d", &progNum);

                        //   printf("Programm '%s' im Verzeichnis '%s' wird gestartet...\n", progName, (allPaths + progNum));
                        //if() mit Parametern...



                    }


                    exit(EXIT_FAILURE);
                }

            } else {

                wait(&status);
                printf("Der Kindprozess mit der ID %d und der StatusID %d hat sich beendet! \n", cpid, status);

            }

        }

        for (int i = 0; i < 10; i++) {
            free(progParams[i]);
           free(progPaths[i]);
        }
        free(progParams);
        free(progPaths);

    }


    return EXIT_SUCCESS;
}















