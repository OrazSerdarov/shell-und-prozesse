
#include <string.h>

#define MAX 255

struct ProgStruct {
    char progName [MAX];
    int numOfPathsFound;
    int paramNum;
    bool end;
    bool containsAbsolutePath;
};

struct EnvPaths {
    char * paths [255];
    int size;
};

void initializeStruct(struct ProgStruct * aProgStruct) {
    aProgStruct->numOfPathsFound = 0;
    aProgStruct->paramNum = 0;
    aProgStruct->end = false;
    aProgStruct->containsAbsolutePath = false;
   memset(aProgStruct->progName, 0, MAX);


}

bool containsAbsolutePath(const char * input) {
    bool con1 = (input[0] == '/');
    bool con2 = (input[0] == '.')&& (input[1] == '/');
    bool con3 = (input[0] == '.')&& (input[1] == '.') && (input[2] == '/');

    return con1 | con2 | con3;
}

void parseInput(struct ProgStruct* aProgStruct, char ** progParams) {

    char * ptr = malloc(MAX * sizeof (char));
    char * input = ptr;
    fgets(input, MAX, stdin);

    char * nextParam = NULL;
    input[strcspn(input, "\n")] = '\0';

    if (strcmp(input, "schluss") == 0) {
        aProgStruct->end = true;
    } else {


        aProgStruct->containsAbsolutePath = containsAbsolutePath(input);
        if (aProgStruct->containsAbsolutePath == true) {
            strsep(&input, "/");
        }

        strcpy(aProgStruct->progName, strsep(&input, " "));

        if (input != NULL) {
            int i = 0;
            while ((nextParam = strsep(&input, " ")) != NULL) {
                strcpy(*(progParams + i), nextParam);
                i++;
            }
            aProgStruct->paramNum = i;
        }






    }

    free(ptr);


}

void parseEnvPath(struct EnvPaths *envPaths) {
    char * env = getenv("PATH");
    char * envCopy = malloc(225 * sizeof (char));
    strcpy(envCopy, env);
    char * aPath;
    int len =0;
    for (int i = 0; (aPath = strsep(&envCopy, ":")) != NULL; i++) {
        len = strlen(aPath);
        aPath[len]='\0';
        strcpy(((envPaths->paths) + i), aPath);
        envPaths->size = (envPaths->size) + 1;
    }
    
    

    //    for(int i =0 ; i<envPaths->size ;i++){
    //               printf("Apath :%s \n" , envPaths->paths +i);
    //        for(int j=0 ;j<strlen(envPaths->paths+ i);j++){
    //            printf("%c " ,(*(*envPaths).paths+i) +j);
    //        }
    //              printf(" \n" ) ;
    //    }






    free(envCopy);

}



char ** findAllProg(struct ProgStruct * aProgStruct, char ** progPaths) {

    struct EnvPaths envPaths;
    envPaths.size = 0;
    envPaths.paths[0] = NULL;
    parseEnvPath(&envPaths);
    for (int i = 0; i< envPaths.size; i++) {
        if (searchInDirectory((envPaths.paths) + i, aProgStruct->progName) == 1) {
            char * aPath = envPaths.paths+i;
            strcpy(*(progPaths+i),aPath);
            aProgStruct->numOfPathsFound = aProgStruct->numOfPathsFound + 1;
         
        }

    }
    
    for(int j=0 ; j< envPaths.size;j++){
           printf("Path %i saved : %s   adr  %p  \n",j, progPaths[j] ,progPaths);
    }
    return progPaths;

}

int searchInDirectory(char * dirname, char *progName) {

    DIR *dir;
    struct dirent *dirp;
    bool containsProg = false;
    dir = opendir(dirname);
    chdir(dirname);

    if (dir != NULL) {


        while ((dirp = readdir(dir)) != NULL) {

            if (dirp->d_type == 4) {
            } else if (strcmp(dirp->d_name, progName) == 0) {
                printf("Prog %s in Ornder %s  vorhanden \n\n", progName, dirname);
                return 1;
            }

        }
    }
    chdir("..");
    closedir(dir);

    return 0;
}
