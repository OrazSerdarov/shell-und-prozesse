#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>


 int counter = 0;

void mySignalhandler(int sigtype){
    if(sigtype==SIGTERM){
          printf("\nSIGTERM ist aufgetreten! \n");
    }
    
    if(sigtype==SIGINT){
          printf("\nSIGINT ist aufgetreten! \n");
        
    }
 
    counter++;

}


int main(int argc, char** argv) {
	printf("Diese Lösung wurde erstellt von Oraz Serdarov \n\n");
    printf("Programm A23 mit der ID %d  wird gestart... \n" , getpid());
    printf("Benutzerdefiniertes Verhalten für die Signale SIGTERM und SIGINT"
            " werden gesetz. \n");
    printf("Senden Sie einer der Signale an dieses Prozess, so wird dies"
            " auf der Konsole ausgegeben. \n"
            "Jedoch maximal 3 Mal. Danach wird Default Verhalten gesetzt. \n");
    
    
    struct sigaction action ;
 
    action.sa_handler =mySignalhandler;
    sigemptyset(&action.sa_mask);
    action.sa_flags=SA_RESTART;
    
    sigaction(SIGINT,&action,NULL);
    sigaction(SIGTERM,&action,NULL);
        
    while(counter<3);

    printf("Prozess(%d) beendet sich jetz...\n", getpid());
    return 5;
}



