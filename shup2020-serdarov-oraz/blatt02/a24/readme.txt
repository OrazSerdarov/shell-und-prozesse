Autor: Oraz Serdarov
Shell und Prozesse SS 2020
Blatt 2: Prozesse unter Linux
Aufgabe 24

Vorbedigung:
Das aktuelle Arbeitzverzeichnis ist:
blatt02/a24

Generieranleitung:
1. Generieren des ausfuehrbaren Programms mit:
gcc a24.c -o a24
2. Generieren des ausfuehrbaren Programms HelloWorld mit:
gcc HelloWorld.c -o HelloWorld

Bedienungsanleitung:
1. Start des ausfuehrbaren Programms als Hintergrundprozess mit:
./a24 &